/*
 * Brett Yeager
 * 12/04/2017
 * SharedLinkQueue.java
 * This class is a thread safe queue that holds links to websites
 */
package crawler;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * This class is a thread safe queue that holds links waiting to be downloaded
 * @author Brett Yeager
 *
 */
public class SharedLinkQueue {
	
	private static Queue<String> sharedLinkQueue = new LinkedList<String>();
	private static int totalLinksFound;
	public static final int MAXIMUM_QUEUE_SIZE = 1000;
	private static HashSet<String> linksFoundSoFarHashed = new HashSet<String>();
	
	/**
	 * Add a link to the queue, if there is no room it will be discarded
	 * @param url The link to add to the queue
	 */
	public static void addLink(String url) throws InterruptedException {
		synchronized(sharedLinkQueue) {
			
			//if there is no room, the link should be discarded
			//if we already found that link, it should be discarded
			if(sharedLinkQueue.size()<MAXIMUM_QUEUE_SIZE && !linksFoundSoFarHashed.contains(url)) {
				if(sharedLinkQueue.size()==0) {
					sharedLinkQueue.notifyAll();
				}
				sharedLinkQueue.add(url);
				linksFoundSoFarHashed.add(url);
				
				totalLinksFound += 1;
			}
		}
		
	}
	
	/**
	 * Get the next link in the queue
	 * @return The next link
	 * @throws InterruptedException
	 */
	public static String getNextLink() throws InterruptedException {
		synchronized(sharedLinkQueue) {
			while(sharedLinkQueue.size()==0) {
				sharedLinkQueue.wait(); //wait for a link to be added
			}
			return sharedLinkQueue.remove();
		}
	}
	
	/**
	 * Gets how many unique links have been found
	 * @return The total number of unique links found
	 */
	public static int getLinksFound() {
		synchronized(sharedLinkQueue) {
			return totalLinksFound;
		}
	}

}
