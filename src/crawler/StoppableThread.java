/**
 * Brett Yeager
 * 12/04/2017
 * StoppableThread.java
 * This interface is used to standardize the ability to stop our Producer and Consumer threads
 */
package crawler;

/**
 * This interface is used to standardize the ability to stop our Producer and Consumer threads
 * @author Brett Yeager
 *
 */
public interface StoppableThread {
	public void stopThread();
}
