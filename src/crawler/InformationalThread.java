/**
 * Brett Yeager
 * 12/04/2017
 * InformationalThread.java
 * This class is used to report information to the user
 */
package crawler;

import java.text.DecimalFormat;

/**
 * This class is used to print useful information about our webcrawler
 * and what the web crawler has found so far
 * @author Brett Yeager
 *
 */
public class InformationalThread extends Thread implements StoppableThread{
	
	private boolean stop;
	
	//We want to format the average to only show two decimal places
	private static DecimalFormat averageFormat = new DecimalFormat("#.00");
	private static final int timeBetweenStatisticPrints = 5000;
	
	/**
	 * Print the statistics and then wait 5 seconds
	 */
	public void run() {
		
		this.setName("Informational Thread");
		
		while (!stop) {
			
			InformationalThread.printStats();
			
		    try {
				Thread.sleep(timeBetweenStatisticPrints);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Print the statistics of our web crawler
	 */
	public static void printStats() {
		System.out.println();
		System.out.println("Individual keyword counts: ");
		
		
		for(String keyword : ParserConsumer.keywordsList) {
			int totalTimesKeywordWasFound = ParserConsumer.getTimesKeywordFound(keyword);
			float average = ParserConsumer.getAverageTimesKeywordFound(keyword);
			
			// Check if we divided by zero pages
			if(Float.isNaN(average)) {
				average = 0;
			}

			System.out.print(keyword + " " + totalTimesKeywordWasFound);
			System.out.print(", " + averageFormat.format(average) + " per page");
			System.out.println();
			
		}

		System.out.println();
		System.out.println("Links found: " + SharedLinkQueue.getLinksFound());
		System.out.println("Pages found: " + SharedPageQueue.getTotalPagesFound());
		System.out.println("Failed downloads: " + FetcherProducer.getTotalFailedDownloads());
		System.out.println("Producers: " + FetcherProducer.getTotalFetcherProducerThreadsStarted());
		System.out.println("Consumers: " + ParserConsumer.getTotalParserConsumerThreadsStarted());
	}

	/**
	 * Stop this thread
	 */
	public void stopThread() {
		stop = true;
	}
	
}
