/*
 * Brett Yeager
 * 12/04/2017
 * FetcherProducer.java
 * This thread class downloads webpages using links from the link queue
 */
package crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import driver.WebCrawler;

/**
 * This thread class downloads webpages using links from the link queue
 * @author Brett Yeager
 *
 */
public class FetcherProducer extends Thread implements StoppableThread{

	private static int totalFetcherProducerThreadsStarted = 0;
	private static int totalFailedDownloads = 0;
	private boolean done;
	
	/**
	 * Run thread to download webpages using links from the link queue
	 */
	public void run() {

		// In case multiple FetcherProducer threads are started at the same time
		synchronized(ParserConsumer.class) {
			totalFetcherProducerThreadsStarted += 1;
			this.setName("Fetcher Producer #" + totalFetcherProducerThreadsStarted);
		}

		//Logger is thread safe
		WebCrawler.LOGGER.info("A fetcher producer has started");
		
		while(!done) {
			
				URL url = null;
				
				boolean downloadSuccessful = false;
				
				Document page;
				try {
					page = Jsoup.connect(SharedLinkQueue.getNextLink()).get();
					SharedPageQueue.addPage(page);
					downloadSuccessful = true;
					
				} catch (IOException e) {
					
					WebCrawler.LOGGER.severe("Download failed: " + e);
					
					downloadSuccessful = false;
					
				} catch (IllegalArgumentException e) {
					WebCrawler.LOGGER.severe("Download Failed: " + e);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				//We didn't add the page so the download wasn't successful
				if(!downloadSuccessful) {
					synchronized(FetcherProducer.class){
						totalFailedDownloads += 1;
					}
				}
				
		}
	}
	
	/**
	 * Get how many FetcherProducer threads have been started
	 * @return How many FetcherProducer threads have been started
	 */
	public static int getTotalFetcherProducerThreadsStarted() {
		return totalFetcherProducerThreadsStarted;
	}
	
	/**
	 * Get how many times a website download failed
	 * @return The amount of times a website download failed
	 */
	public static int getTotalFailedDownloads() {
		return totalFailedDownloads;
	}
	
	/**
	 * Stop this thread
	 */
	public void stopThread()
    {
        done = true;
    }
	
}
