/*
 * Brett Yeager
 * 12/04/2017
 * SharedPageQueue.java
 * This class is a thread safe queue that holds pages waiting to be parsed
 */
package crawler;

import java.util.LinkedList;
import java.util.Queue;

import org.jsoup.nodes.Document;

/**
 * This class is a thread safe queue that holds pages waiting to be parsed
 * @author Brett Yeager
 *
 */
public class SharedPageQueue {
	
	// Document class is from org.jsoup.nodes
	private static Queue<Document> sharedPageQueue = new LinkedList<Document>();
	private static int totalPagesFound;
	public static final int MAXIMUM_QUEUE_SIZE = 100;
	
	/**
	 * Add a page to the queue
	 * @param pageText The page to add
	 */
	public static void addPage(Document pageText) throws InterruptedException {
		synchronized(sharedPageQueue) {
			
			//page gets discarded if there isn't any room in the queue
			if(sharedPageQueue.size()<MAXIMUM_QUEUE_SIZE) 
			{
				
				if(sharedPageQueue.size()==0) {
					sharedPageQueue.notifyAll();
				}
				
				//The page must go through the queue before being parsed
				sharedPageQueue.add(pageText);
				totalPagesFound += 1;
				
			}
		}
	}
	
	/**
	 * Get the next page in the queue
	 * @return The next page
	 */
	public static Document getNextPage() throws InterruptedException {
		
		synchronized(sharedPageQueue) {
			
			while(sharedPageQueue.size()==0) {
				sharedPageQueue.wait();	
			}
			
			return sharedPageQueue.remove();
		}
	}
	
	/**
	 * Get how many pages have been added
	 * @return the total number of pages added to the queue
	 */
	public static int getPagesDownloaded() {
		
		synchronized(sharedPageQueue) {
			return sharedPageQueue.size();
		}
		
	}
	
	/**
	 * Get how many pages have been added to the queue
	 * @return How many pages have been added
	 */
	public static int getTotalPagesFound() {
		return totalPagesFound;
	}

}
