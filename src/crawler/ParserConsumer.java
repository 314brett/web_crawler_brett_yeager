/*
 * Brett Yeager
 * 12/04/2017
 * ParserConsumer.java
 * This Thread class parses websites for information
 * about keywords and links it contains
 */
package crawler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import driver.WebCrawler;

/**
 * This class parses websites to get information about keywords and
 * links it contains
 * @author Brett Yeager
 *
 */
public class ParserConsumer extends Thread implements StoppableThread {
	
	private static Pattern pattern = Pattern.compile("href=\"(http:.*?)\"");
	private static int totalParserConsumerThreadsStarted = 0;
	private static HashMap<String, Integer> keywordCounts = new HashMap<>();
	
	private static HashMap<String, Integer> totalPagesWithKeyword = new HashMap<>();
	
	public static ArrayList<String> keywordsList = new ArrayList<String>();
	
	private boolean done;

	
	/**
	 * Run thread to parse the next page
	 */
	public void run() {
		
		// In case multiple ParserConsumer threads are started at the same time
		synchronized(ParserConsumer.class) {
			totalParserConsumerThreadsStarted += 1;
			this.setName("Parser Consumer #" + totalParserConsumerThreadsStarted);
		}
		
		//Logger is thread safe
		WebCrawler.LOGGER.info("A parser consumer has started");
		
		while(!done) {
				Document pageText;
				try {
					pageText = SharedPageQueue.getNextPage();

					try {
						
						//Get all of the links from the page
						addLinksFromPage(pageText);
						
					} catch (InterruptedException e) {
						WebCrawler.LOGGER.info(e.getStackTrace().toString());
					}
					
					//Count all of the keywords in the page
					incremenetKeywordCounts(pageText);
					
				} catch (InterruptedException e1) {
					
					WebCrawler.LOGGER.info(e1.getStackTrace().toString());
				}
				
					
		}
		
	}
	
	private void addLinksFromPage(Document givenPage) throws InterruptedException {
		Elements links = givenPage.select("a[href]");
		
		//Finds all of the links in the page and adds them to the link queue
		for (Element link : links)
		{
		    SharedLinkQueue.addLink(link.absUrl("href"));
		}
		
	}
	
	private void incremenetKeywordCounts(Document givenPage) {
		
		String givenPageText = givenPage.text();
		
		//Check the document for each of the keywords
		for(int i=0; i<keywordsList.size(); i++) 
		{
			String currentKeyword = keywordsList.get(i);
			
			// "(?i)" makes this split case insensitive
			String[] parts = givenPageText.split("(?i)" + currentKeyword);
			
			//The totalKeywordsFound int is shared among our ParserConsumer threads
			synchronized(ParserConsumer.class) {
				keywordCounts.put(currentKeyword, keywordCounts.get(currentKeyword) + parts.length - 1);
				
				//If the keyword was found on this page, this is another page with that keyword
				if(parts.length>1) {
					totalPagesWithKeyword.put(currentKeyword, totalPagesWithKeyword.get(currentKeyword) + 1);
				}
				
			}
			
		}
	}
	
	/**
	 * Get how many ParserConsumer threads have been started
	 * @return how many ParserConsumer threads have been started
	 */
	public static int getTotalParserConsumerThreadsStarted() {
		return totalParserConsumerThreadsStarted;
	}
	
	/**
	 * Get how many time the keyword was found
	 * @return the total number of times keyword was found
	 */
	public static int getTimesKeywordFound(String keyword) {
		return keywordCounts.get(keyword);
	}
	
	/**
	 * Get the average amount of times the keyword was found on pages it was actually found
	 * @param keyword the search term
	 * @return The average amount of times the keyword was found
	 */
	public static float getAverageTimesKeywordFound(String keyword) {
		return (1f * keywordCounts.get(keyword) / totalPagesWithKeyword.get(keyword));
	}
	
	/**
	 * Add a new keyword to our search
	 * @param newKeyword the keyword to count in the pages we find
	 */
	public static void addKeyword(String newKeyword) {
		
		//make sure we haven't already added that keyword
		if(!keywordCounts.containsKey(newKeyword)) {
			ParserConsumer.keywordsList.add(newKeyword);
			ParserConsumer.keywordCounts.put(newKeyword, 0);
			totalPagesWithKeyword.put(newKeyword, 0);
		}
	}

	/**
	 * Stop this thread
	 */
	public void stopThread()
    {
        done = true;
    }
}
