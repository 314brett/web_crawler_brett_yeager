/*
 * Brett Yeager
 * 12/04/2017
 * WebCrawler.java
 * This class tests our web crawler with a user interface
 */
package driver;

import java.awt.List;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import crawler.FetcherProducer;
import crawler.InformationalThread;
import crawler.ParserConsumer;
import crawler.SharedLinkQueue;
import crawler.StoppableThread;

/**
 * This class tests the web crawler with a console interface menu
 * @author Brett Yeager
 */
public class WebCrawler {
	
	public static final int MENU_ADD_SEED_URL = 1;
	public static final int MENU_ADD_CONSUMER = 2;
	public static final int MENU_ADD_PRODUCER = 3;
	public static final int MENU_ADD_KEYWORD_SEARCH = 4;
	public static final int MENU_PRINT_STATS = 5;
	public static final int MENU_INFORMATIONAL_THREAD = 6;
	public static final int MENU_Exit = 7;
	
	public static ArrayList<StoppableThread> threadsStarted = new ArrayList<StoppableThread>();
	
	public static final Logger LOGGER = Logger.getLogger(WebCrawler.class.toString());
	

	public static final String MENU_MAIN_STATEMENT = MENU_ADD_SEED_URL + ". Add seed url\r\n" + 
			MENU_ADD_CONSUMER + ". Add consumer\r\n" + 
			MENU_ADD_PRODUCER + ". Add producer\r\n" + 
			MENU_ADD_KEYWORD_SEARCH + ". Add keyword search\r\n" + 
			MENU_PRINT_STATS + ". Print stats\r\n" + 
			MENU_INFORMATIONAL_THREAD + ". Informational Thread\r\n" +
			MENU_Exit + ". Exit Program";
	
	/**
	 * Tests our web crawler
	 */
	public static void main(String[] args) throws InterruptedException {
		
		setupLogger();
		
		doMainMenu();
		
	}
	
	private static void setupLogger() {
		try
		{
			LOGGER.addHandler(new FileHandler("WebCrawler.log"));
			LOGGER.setUseParentHandlers(false);
		}
		catch (Exception e)
		{
			System.out.println("Error setting up log file.");
		}
	}
	
	private static void doMainMenu() throws InterruptedException {
		
		Scanner userInput = new Scanner(System.in);
		int selectedMenuChoice = 0;
		
		while(selectedMenuChoice!=MENU_Exit) {
			
			System.out.println(MENU_MAIN_STATEMENT);
			
			try {
				selectedMenuChoice = userInput.nextInt();
				
				userInput.nextLine();
				
				switch(selectedMenuChoice) {
					case MENU_ADD_SEED_URL:
						addSeedURLMenu(userInput);
						break;
					case MENU_ADD_CONSUMER:
						addConsumer(userInput);
					break;
					case MENU_ADD_PRODUCER:
						addProducer(userInput);
					break;
					case MENU_ADD_KEYWORD_SEARCH:
						addKeywordSearchMenu(userInput);
						break;
					case MENU_PRINT_STATS:
						InformationalThread.printStats();
						break;
					case MENU_INFORMATIONAL_THREAD:
						startInformationalThread();
					break;
					case MENU_Exit:
						exitProgram();
					break;
				}
			}catch(InputMismatchException e){
				System.out.println("Oops: please enter a number");
				LOGGER.warning("Menu input error: " + e);
				
				// It immediately went to error after userInput.nextInt();
				userInput.nextLine();
			}
			
		}
		
		userInput.close();
		
	}
	
	private static void addSeedURLMenu(Scanner userInput) throws InterruptedException {
		
		//Add a seed url to the shared link queue.
		
		System.out.print("Enter url: ");
		String seedURL = userInput.nextLine();	
		WebCrawler.LOGGER.info("A new seed URL has been added: " + seedURL);
		SharedLinkQueue.addLink(seedURL);
	}
	
	private static void addConsumer(Scanner userInput) {

		//Start up a parse thread
		ParserConsumer parserConsumer = new ParserConsumer();
		threadsStarted.add(parserConsumer);
		parserConsumer.start();
	}
	
	private static void addProducer(Scanner userInput) {

		//Start up a fetcher thread
		FetcherProducer fetcherProducer = new FetcherProducer();
		threadsStarted.add(fetcherProducer);
		fetcherProducer.start();
		
	}
	
	private static void addKeywordSearchMenu(Scanner userInput) {
		System.out.print("Enter search term: ");
		String searchTerm = userInput.nextLine();
		ParserConsumer.addKeyword(searchTerm);
	}
	
	private static void startInformationalThread() {
		InformationalThread informationalThread = new InformationalThread();
		threadsStarted.add(informationalThread);
		informationalThread.start();
	}
	
	private static void exitProgram() {
		
		for(StoppableThread stoppableThread : threadsStarted) {
			stoppableThread.stopThread();
		}
		
	}
	
}
