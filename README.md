# README #

This Java program is a basic web crawler that starts with a seed URL and finds connected web sites.  
It uses producer threads to download web pages and consumer threads to 
parse the pages for links.

### Instructions ###
First enter a seed url and optionally enter search terms.  
Then, start up producer and consumer threads  

The producer threads download web pages.  
The consumer threads parse the web pages using jsoup and then looks for links on that page. 

### Example seed URL's: ###
* `http://cnn.com`  
* `http://disney.com`  

### Contact Brett Yeager ###

* 3.14brett@gmail.com